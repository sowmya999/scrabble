Scrabble
--------

Problem ::

   Given a rack of seven letters, determine the highest scoring word that can be formed out of these seven. The words are scored by adding the score of individual letter as per Scrabble rules.

Input ::

   1. The rack of seven letters. Accepted from the commandline. case not known

   2. Scoring table

   3. Official word list ::

       * One word per line

       * includes newlines

       * length 2 -- 15

       * in alphabetical order

       * in upper case

Output ::

   A list of valid words, sorted by score -- descending.


Process ::

   1. Assume valid input of a rack.

   #. Generate possible strings of length 2 - 7 from the rack.
   
   #. To check whether the generated strings are valid or not.
   
   #. Then generate a score for the valid string.

   #. Display all the words followed by the score in descending order.
