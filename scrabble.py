from operator import itemgetter
import sys
import itertools

dic={}
def words_list():
    sowpods=[]
    for line in open("/home/bvrith/Desktop/sowpods.txt"):
        sowpods.append(line.strip())
    return sowpods

def permutations():
    possible_words=[]       
    for size in range(2,8):
        for w in itertools.permutations(rack,size):
            word = "".join(w)
            if word not in possible_words:
	        possible_words.append(word)
    return possible_words

def valid_words(possible_words,sowpods):
    valid_words_list=[]     
    for word in possible_words:
        low=0
        high=len(sowpods)
        while low <= high:
            mid=(low+high)/2
            if(word==sowpods[mid]):
                valid_words_list.append(word)
                validword_scores(word.lower())
                break
            elif(word < sowpods[mid]):
                high = mid - 1
            else:
                low = mid + 1

def validword_scores(word):
    score_array=[]
    scores = {"a": 1, "c": 3, "b": 3, "e": 1, "d": 2, "g": 2,"f": 4, "i": 1, "h": 4, "k": 5, "j": 8, "m": 3,"l": 1, "o": 1, "n": 1, "q": 10, "p": 3, "s": 1,"r": 1, "u": 1, "t": 1, "w": 4, "v": 4, "y": 4,"x": 8, "z": 10}
    score = 0
    for letters in word:
        score = score + scores[letters]
    dic[word]=score
    score_array.append(score) 
    return score_array
rack = sys.argv[1].strip().upper()
if len(rack) < 7 or len(rack) >7:
    print "enter a 7 letter word"
    exit(0)
sowpods=[]
possible_words=[]
sowpods=words_list()
possible_words=permutations()
valid_words(possible_words,sowpods)
valid_lis=[]
for scrap in dic:
    valid_lis.append([dic[scrap],scrap])
valid_lis = sorted(valid_lis,key = itemgetter(0))
valid_lis.reverse()
for word in valid_lis:
    print str(word[0])+" "+word[1]
                                                                                                                                                                                                                                                                                                                                                                                                
